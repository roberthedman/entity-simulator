
# Entity Simulator
This is a small weekend project for fun and playground.
It's written in c++ and uses SFML for graphics.
I happen to have two GPUs so if this thing takes a ML turn then those will come in handy too, probably using OpenCL as they're not Nvidia GPUs...

## Rough idea
is to simulate various "Entities" in a flat world.
Enteties are designed to be overridden and have special traits, like SeeingEntity.
These Entities are inherit from Simulatable class.
The other type of simulatables are consumables, like apples.
Entities eat apples for more energy.
Entities die when out of energy.
Moving takes energy.

When there are few enough entities left the remaining ones are reset, given full health and then cloned.
Clones have a varying degree of brain mutations.

Let the games begin.

### List of future ideas:
- Need to add walls so simulation world, or edge indication for falling of the edge of the world. Currently entities are strongly biased to just rotate around when not seeing any food due to the lack of walls. Entities who actually go out hunting are statistically biased to leave the area with food.
- Standardize neural nets even further
- Implement activation functions and other cool feutures in the brain.
	- Maybe add an array of input copies which hold the previous inputs in order, so that entities can have a memory for a certain number of steps.
- Add controls and information on screen regarding generation.
	- Maybe allow custom selection for next generation
	- Add controls for simulation speed, number in generation at start, etc
	- Some idnetification in color of how similar entities brains are
- brain dump button, probably into JSON.
	- Custom start entries
- Maybe have predator/prey thing going on
- Maybe use other means of neural net evolving or ML techniques.
- Maybe convert into a 3D world
- Maybe have various source of food
- Maybe have various land areas and resources / habitats suitable for various entities
- Maybe have sprint "health" and similar features
	- Make non linear cost of moving with regards to speed, and in 3D case jumping.
- Maybe have life expectancy, reproducability probability, mating, age, speed, etc, dependencies.

