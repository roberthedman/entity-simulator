/*
 * main.cpp
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#include "Simulation/Simulator.h"
#include "Simulatables/Entity.h"
#include "Simulatables/Apple.h"
#include "Simulatables/SeeingEntity.h"

#include "Neural/NeuronInput.h"
#include "Neural/Neuron.h"
#include "Neural/NeuralLayer.h"
#include "Neural/NeuralBrain.h"

#include <iostream>

int main() {

	SeeingEntity::loadFont();
	std::cout << "Initializing simulator..." << std::endl;
    Simulator sim = Simulator();
    std::cout << "Done! Adding entities..." << std::endl;
    //sim.addEntity(new Entity("Vegetable1", 100, 100));
    //sim.addEntity(new Entity("Vegetable2", 200, 200));
    for(int i=0;i<10;i++){ //10
    	for(int j=0;j<7;j++){ //7
    		std::cout << "Adding entity..." << std::endl;
    		sim.addEntity(std::make_unique<SeeingEntity>(SeeingEntity(sim.getVisibilityManager(), "Seer"+std::to_string(i)+std::to_string(j), 80*i+350, 250+120*j)));
    	}
    }
    std::cout << "Done adding entities! Adding apples..." << std::endl;
    sim.populateApples();
    std::cout << "Done adding apples! Starting simulation..." << std::endl;

    while (sim.isWindowOpen()) {
        sf::Event event;
        while (sim.getWindow().pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                sim.getWindow().close();
        }
        sim.simulateStep(); // Simulate and draw entities
    }

    return 0;
}
