/*
 * Entity.h
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include "Simulatables/Simulatable.h"
#include <random>
#include <string>
#include "Math/RandomDist.h"

class Entity : public Simulatable {
private:
	RandomDist dist;
protected:
	std::string name;
    float x, y;
    bool alive;
    float energy;
    float speed;
	int generation;
    const float maxSpeed = 0.25;
    float moveEnergyCost;
    float ageCost;
    float passiveCost;
    float currentDirection;
    float directionDampening;
    sf::CircleShape body;
    sf::CircleShape viewingArea;
    void setSpeed(float s);
    void setDirection(float d);
    void move();
    float getRandomAngle();

    static const int bodyRadius = 6; // An entity of same kind should not have differing shapes.
    static const int viewingRadius = 10*bodyRadius; // 10 times its size seems like a good starting guess for good simulations

    virtual void updateGraphicalPositions();
    virtual sf::Color generationColor();

public:
    Entity(std::string name, float x, float y);
    void step() override;
    void draw(sf::RenderWindow& window) override;
    void setPosition(float x, float y) override;
    bool isAlive() const override;
    sf::Vector2f getPosition() const override;
    const sf::CircleShape& getBoundingCircle() const override;
    bool consumable() override;
    float consume() override;
    void reset() override;
	void incrementGeneration();
	int getGeneration();
	void updateColor();
};



#endif /* ENTITY_H_ */
