/*
 * Apple.cpp
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#include "Apple.h"
#include <cmath>

Apple::Apple(float x, float y) :
	body(), x(x), y(y), nuitrition(1.0), greeness(1.0), isConsumed(false){
	body.setRadius(bodyRadius);
	body.setOrigin(bodyRadius, bodyRadius);
	body.setFillColor(sf::Color(0, 255, 50));
}

void Apple::reset(){
	x=0;
	y=0;
	nuitrition=1;
	greeness=1;
	isConsumed=false;
	body.setFillColor(sf::Color(0, 255, 50));
}

void Apple::step(){
	// Apples perish over time, turn brown and loose nutrition.
	float reductionSpeed = 0.0001;
	nuitrition -= reductionSpeed;
	greeness -= reductionSpeed;
	greeness = std::max(0.0f, std::min(greeness, 1.0f)); // Clamp greeness between 0 and 1
	sf::Uint8 greenComponent = static_cast<sf::Uint8>(std::round(greeness * 255.0f));

	body.setFillColor(sf::Color(0, greenComponent, 50));

}

void Apple::draw(sf::RenderWindow& window){
	body.setPosition(x, y);
	window.draw(body);
}

void Apple::setPosition(float x, float y){
	this->x = x;
	this->y = y;
}

float Apple::consume(){
	isConsumed = true;
	return nuitrition;
}

bool Apple::isAlive() const {
	return !isConsumed && nuitrition>0;
}

sf::Vector2f Apple::getPosition() const {
	return sf::Vector2f(x,y);
}

const sf::CircleShape& Apple::getBoundingCircle() const {
	return body;
}
