/*
 * Entity.cpp
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#include "Entity.h"
#include <vector>
#include <cmath>
#include <iostream>


Entity::Entity(std::string name, float x, float y):
	dist(-M_PI, M_PI),
	name(name),
	x(x),
	y(y),
	alive(true),
	energy(1.0),
	speed(maxSpeed),
	generation(0),
	moveEnergyCost(0.0008), //0.002
	ageCost(0.002),
	passiveCost(0.005),
	currentDirection(M_PI/1.75),
	directionDampening(0.02),
	body(),
	viewingArea()
{
	// for temporary debugging/ optimizing allow random energies
	energy -= 0.01*dist.getRnd();
	// Setup body graphics
	body.setRadius(bodyRadius);
	body.setOrigin(bodyRadius, bodyRadius); // We want shapes (circles) in this entity to be references from their center.
	// Setup viewing area graphics for body
	viewingArea.setRadius(viewingRadius);
	viewingArea.setOrigin(viewingRadius, viewingRadius);
	// set colors
	body.setFillColor(sf::Color::Cyan); // Light blue color
	viewingArea.setFillColor(sf::Color(0, 0, 255, 50)); // semi-transparent blue
}

void Entity::reset(){
	x=0;
	y=0;
	alive=true;
	energy=1.0;
	currentDirection = M_PI/1.75;
	incrementGeneration();
}
void Entity::incrementGeneration(){
	++generation;
	body.setFillColor(generationColor());
}

int Entity::getGeneration(){
	return generation;
}


float Entity::getRandomAngle() {
	return dist.getRnd();
}

void Entity::setPosition(float x, float y){
	this->x = x;
	this->y = y;
	//updateGraphicalPositions();
}
void Entity::updateGraphicalPositions() {
	body.setPosition(x, y);
	viewingArea.setPosition(x,y);
 }

void Entity::step() {
	setDirection( currentDirection*directionDampening +
			(1-directionDampening)*(currentDirection+getRandomAngle()));
	setSpeed(speed);
	move();
}

void Entity::move() {
	float dx = cos(currentDirection) * speed;
	float dy = sin(currentDirection) * speed;
	float distanceMoved = sqrt(dx*dx + dy*dy);
	energy -= distanceMoved*moveEnergyCost + ageCost;
	if(distanceMoved < 0.5){
		energy -= passiveCost;
	}
	//TODO: check if energy is 0, die? restrict movement?
	x += dx;
	y += dy;
	setPosition(x,y);
}

void Entity::setSpeed(float s) {
	speed = s;
}
void Entity::setDirection(float d){
	currentDirection = d;
}

void Entity::draw(sf::RenderWindow& window) {
	updateGraphicalPositions();
	window.draw(body);
    window.draw(viewingArea);
}

bool Entity::isAlive() const {
	return alive && energy>0;
}

bool Entity::consumable() {
	return false; // I cannot be eaten! At least not for now.
}

float Entity::consume() {
	return -10.0; // I cannot be eaten! At least not for now.
}

sf::Vector2f Entity::getPosition() const {
	return sf::Vector2f(x,y);
}

const sf::CircleShape& Entity::getBoundingCircle() const {
	return body;
}

void Entity::updateColor(){
	body.setFillColor(generationColor());
}

sf::Color Entity::generationColor(){
	return sf::Color::Cyan;
}

