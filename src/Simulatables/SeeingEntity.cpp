/*
 * SeeingEntity.cpp
 *
 *  Created on: Dec 25, 2023
 *      Author: robert
 */

#include "Simulatables/SeeingEntity.h"
#include "Math/VecOp.h"

#include <iostream>

sf::Font SeeingEntity::font;
void SeeingEntity::loadFont(){
	if (!SeeingEntity::font.loadFromFile("/usr/share/fonts/truetype/freefont/FreeSans.ttf"))
	{std::cout << "MAJOR ISSUE" << std::endl;}
}


SeeingEntity::SeeingEntity(VisibilityManager &vm, std::string name, float x, float y, NeuralBrain b) :
Entity(name, x, y),
vm(vm),
brain(b),
sightBeam(),
sightDistance(200.0f),
targetLocked(false),
text()
{
	body.setFillColor(generationColor());
	float beamWidth = 2.0;
	viewingArea.setFillColor(sf::Color(120, 0, 150, 0));

	directionDampening=0.93;
	speed = maxSpeed;

	sightBeam.setSize(sf::Vector2f(sightDistance, beamWidth));
	sightBeam.setFillColor(sf::Color(255, 255, 255, 70));
	sightBeam.setOrigin(0, beamWidth/2.0);
	sightBeam.setPosition(x,y);

	text.setString("HEHEYE");
	text.setFont(font); // WHy isnt this persistent when font isnt static?????
	text.setPosition(x,y);
	text.setCharacterSize(10);
	text.setFillColor(sf::Color::Black);
	//brain.dumpState();

}

void SeeingEntity::step(){

	VisibilityInfo vi = vm.getFirstVisibleObjectAlongLine(getPosition(), getPosition()+sightDistance*getDirection());
	if (vi.anything) { // We are seeing something!
		targetLocked=true;
		if (vi.distance < reachDistance){
			if( vi.entity->consumable() ){
				energy+=0.8*vi.entity->consume(); // 20% is wasted during digestion...?
				//if(energy > 2.0) { energy = 2.0;} // clamp max energy to twice initial. // Mauybe not cap to improve greediness?
			}
		}
	} else {
		targetLocked=false;
		//setDirection( currentDirection*directionDampening +
		//		(1-directionDampening)*(currentDirection+getRandomAngle()));
	}

	// feed brain the inputs we have
	const std::vector<float> inputs = {
			energy,
			static_cast<float>(vi.anything),
			vi.distance/sightDistance,
			vi.color.r / 255.0f,
			vi.color.g / 255.0f,
			vi.color.b / 255.0f,
			1.0f,				// always on neuron
			0.0f,				// always off neuron
		   -1.0f};				// always negative on neuron
	brain.setInputs(inputs);
	brain.stepNet();
	// retrieve the brain outputs
	const std::vector<float>& outputs = brain.getOutputs();
	float bspeed = outputs[0]-outputs[1];
	setSpeed(bspeed*maxSpeed*10);
	float bturn = M_PI*(outputs[2]-outputs[3]);
	energy -= abs(bturn*turnCost);
	setDirection( currentDirection*directionDampening + (1-directionDampening)*(currentDirection+bturn) );
	text.setString(std::to_string(energy)+"\nGen:"+std::to_string(generation)+"\n");

	move();
	//brain.dumpState();
}

void SeeingEntity::draw(sf::RenderWindow& window) {
	updateGraphicalPositions();
	Entity::draw(window);
	window.draw(sightBeam);
	window.draw(text);
}

void SeeingEntity::updateGraphicalPositions(){
	Entity::updateGraphicalPositions();
	sightBeam.setPosition(x,y);
	sightBeam.setRotation(currentDirection*360/(2*M_PI));
	text.setPosition(x,y);
}

sf::Vector2f SeeingEntity::getDirection() {
	return VecOp::getUnitVecFromRad(currentDirection);
}

NeuralBrain& SeeingEntity::getBrain(){ return brain; }

NeuralBrain SeeingEntity::getMutatedBrain(float amount){
	NeuralBrain mb = brain;
	mb.mutate(amount);
	return mb;
}

sf::Color SeeingEntity::generationColor(){
	switch(generation) {
	  case 0:
		  return sf::Color(120, 0, 150);
	  case 1:
		  return sf::Color(200, 0, 150);
	  case 2:
	  	  return sf::Color(50, 200, 50);
	  case 3:
	  	  return sf::Color(30, 200, 200);
	  case 4:
		  return sf::Color(20, 0, 150);
	  case 5:
		  return sf::Color(20, 0, 250);
	  default:
		  return sf::Color(255, 255, 255);
	    // code block
	}
}



