/*
 * Apple.h
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#ifndef SRC_APPLE_H_
#define SRC_APPLE_H_

#include "Consumable.h"
//#include "Entity.h"

class Apple : public Consumable {
private:
	sf::CircleShape body;
	static const int bodyRadius = 3; // Fixed radius for each entity
	float x,y;
	float nuitrition;
	float greeness;
	bool isConsumed;
public:
	Apple(float x, float y);
	void step() override; 							// simulate behaviour for one time step
	void draw(sf::RenderWindow& window) override;	// how to draw thing to screen
	void setPosition(float x, float y) override;
	float consume() override;
	bool isAlive() const override;
	sf::Vector2f getPosition() const override;
	const sf::CircleShape& getBoundingCircle() const override;
	void reset() override;
	~Apple() {};
};




#endif /* SRC_APPLE_H_ */
