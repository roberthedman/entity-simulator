/*
 * SeeingEntity.cpp
 *
 *  Created on: Dec 25, 2023
 *      Author: robert
 */

#ifndef SRC_SIMULATABLES_SEEINGENTITY_H_
#define SRC_SIMULATABLES_SEEINGENTITY_H_

#include "Simulatables/Entity.h"
#include "Simulation/VisibilityManager.h"
#include "Neural/NeuralBrain.h"

class SeeingEntity : public Entity {
private:
	VisibilityManager &vm;
	static const short numInputs = 9;
	static const short numOutputs = 4;
	NeuralBrain brain;
	sf::RectangleShape sightBeam;
	float sightDistance;
	float reachDistance = 10.0;
	float turnCost = 0.01;
	sf::Vector2f getDirection();
	bool targetLocked;
	float maxSpeed = 1.0; //0.08;
	static sf::Font font; // making this static fixed the issue. I am not convinced nor happy about it. WHAT!? I mean, it makes sense from a performance perspective as we dont really need different fonts for each entity, but why shouldnt we be able to??
	sf::Text text;
protected:
	sf::Color generationColor() override;
public:
	SeeingEntity(VisibilityManager &vm, std::string name, float x, float y, NeuralBrain b = NeuralBrain(numInputs, numOutputs));
	void draw(sf::RenderWindow& window) override;
	void updateGraphicalPositions() override;
	void step() override;
	static void loadFont();
	NeuralBrain& getBrain();
	NeuralBrain getMutatedBrain(float amount);
};

#endif
