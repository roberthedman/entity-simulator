/*
 * Simulatable.h
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#ifndef SIMULATABLE_H_
#define SIMULATABLE_H_

#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
//#include "VisibilityManager.h"

class Simulatable {
public:
    virtual void step() = 0; 							// simulate behaviour for one time step
    virtual void draw(sf::RenderWindow& window) = 0;	// how to draw thing to screen
    virtual void setPosition(float x, float y) = 0;
    virtual bool isAlive() const = 0;
    virtual sf::Vector2f getPosition() const = 0;
    virtual const sf::CircleShape& getBoundingCircle() const = 0;
    virtual bool consumable() = 0;
    virtual float consume() = 0;
    virtual void reset() =0;

    virtual ~Simulatable() {}
};


#endif /* SIMULATABLE_H_ */
