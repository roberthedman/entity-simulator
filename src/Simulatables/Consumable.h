/*
 * Consumable.h
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#ifndef SRC_CONSUMABLE_H_
#define SRC_CONSUMABLE_H_

#include "Simulatable.h"
//#include "Entity.h"

class Consumable : public Simulatable {
public:
    bool consumable() override {return true;};
    virtual float consume() = 0;
    virtual ~Consumable() {}
};

#endif /* SRC_CONSUMABLE_H_ */
