/*
 * RandomDist.h
 *
 *  Created on: Dec 30, 2023
 *      Author: robert
 */

#ifndef RANDOMDIST_H_
#define RANDOMDIST_H_

#include <random>

class RandomDist {
private:
    static std::random_device rd;
    static std::mt19937 rng;
    std::uniform_real_distribution<float> dist; // uni_dist(0.0, 1.0);
public:
    RandomDist(float start, float end);
    float getRnd();
};



#endif /* RANDOMDIST_H_ */
