/*
 * RandomDist.cpp
 *
 *  Created on: Dec 30, 2023
 *      Author: robert
 */

#include "RandomDist.h"

std::random_device RandomDist::rd;
std::mt19937 RandomDist::rng(rd());


RandomDist::RandomDist(float start, float end): dist(start, end) {}

float RandomDist::getRnd(){
	return dist(rng);
}


