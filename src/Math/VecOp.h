/*
 * VecOp.h
 *
 *  Created on: Dec 29, 2023
 *      Author: robert
 */

#ifndef SRC_VECOP_H_
#define SRC_VECOP_H_

#include "SFML/System/Vector2.hpp"


class VecOp{
public:
	static float dot(const sf::Vector2f& a, const sf::Vector2f& b);
	static float length(const sf::Vector2f& a);
	static sf::Vector2f normalize(const sf::Vector2f& a);
	static sf::Vector2f getUnitVecFromRad(const float& angle);

};

#endif /* SRC_VECOP_H_ */
