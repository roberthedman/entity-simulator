/*
 * VecOp.cpp
 *
 *  Created on: Dec 29, 2023
 *      Author: robert
 */

#include "VecOp.h"
#include <cmath>

sf::Vector2f VecOp::normalize(const sf::Vector2f &v){
	return v / float(sqrt(dot(v,v)));
}
float VecOp::length(const sf::Vector2f &v){
	return sqrt(dot(v,v));
}

float VecOp::dot(const sf::Vector2f& a, const sf::Vector2f& b){
    return a.x * b.x + a.y * b.y;
}

sf::Vector2f VecOp::getUnitVecFromRad(const float& angle) {
	return normalize(
			sf::Vector2f( cos(angle) , sin(angle) )
			);
}
