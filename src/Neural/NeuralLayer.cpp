/*
 * NeuralLayer.cpp
 *
 *  Created on: Jan 1, 2024
 *      Author: robert
 */

#include "NeuralLayer.h"


//NeuralLayer::NeuralLayer(): size(0){}

NeuralLayer::NeuralLayer(unsigned short numNeurons):neurons(numNeurons), size(numNeurons){ // for reserving vector size
	neurons.reserve(size);
	for(unsigned short i=0; i<size;i++){
		neurons.push_back(Neuron());
	}
}
unsigned short NeuralLayer::getSize() const{ // returns size of neurons vector.
	return size;
	//return neurons.size();
}

Neuron& NeuralLayer::getNeuron(unsigned short index){
	return neurons[index];
}
