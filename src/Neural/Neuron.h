/*
 * Neron.h
 *
 *  Created on: Jan 1, 2024
 *      Author: robert
 */

#ifndef SRC_NEURAL_NEURON_H_
#define SRC_NEURAL_NEURON_H_

#include <vector>
#include <memory>

#include "NeuronInput.h"
#include "Math/RandomDist.h"

class Neuron {
private:
	float state;
	std::vector<NeuronInput> inputs;
	RandomDist dist;

public:
	Neuron();
	void updateState(); // To process all inputs into new state.
	float getState();  // For connections to retrieve and forward to downstream neurons.

	unsigned short getNumInputs(); // returns size of inputs
	void addInput();
	NeuronInput& getNeuronInput(unsigned short index); // For setting/getting value, weight
};



#endif /* SRC_NEURAL_NEURON_H_ */
