/*
 * Neuron.cpp
 *
 *  Created on: Jan 1, 2024
 *      Author: robert
 */

#include "Neuron.h"
#include <algorithm>


Neuron::Neuron(): state(0.0), inputs(), dist(-1.0f,1.0f) {}

void Neuron::updateState(){ // To process all inputs into new state.
	state = 0.0;
	for(auto& i : inputs){
		state+=i.getWeightedValue();
	}
	state = std::clamp(state, -1.0f, 1.0f);
}

float Neuron::getState(){  // For connections to retrieve and forward to downstream neurons.
	return state;
}

unsigned short Neuron::getNumInputs(){ // returns size of inputs
	return inputs.size();
}
void Neuron::addInput(){
	inputs.push_back(NeuronInput(dist.getRnd()));
}

NeuronInput& Neuron::getNeuronInput(unsigned short index){ // For setting/getting value, weight
	return inputs[index];
}
