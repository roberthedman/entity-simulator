/*
 * NeuronInput.h
 *
 *  Created on: Jan 1, 2024
 *      Author: robert
 */

#ifndef SRC_NEURAL_NEURONINPUT_H_
#define SRC_NEURAL_NEURONINPUT_H_

class NeuronInput {
private:
	float weight;
	float value;
public:
	NeuronInput();
	NeuronInput(float weight);
	NeuronInput(float weight, float value);
	// get state
	float getWeightedValue() const;
	float getUnweightedValue() const;
	//set state
	void setInputValue(float input);
	void setWeight(float w);
	// get params
	float getWeight() const;
};


#endif /* SRC_NEURAL_NEURONINPUT_H_ */
