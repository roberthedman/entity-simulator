/*
 * NeuronInput.cpp
 *
 *  Created on: Jan 1, 2024
 *      Author: robert
 */


#include "NeuronInput.h"
#include <iostream>

NeuronInput::NeuronInput(): weight(0.0f), value(0.0f){};
NeuronInput::NeuronInput(float weight): weight(weight), value(0.0f){};
NeuronInput::NeuronInput(float weight, float value): weight(weight), value(value){};


float NeuronInput::getWeightedValue() const{
	//std::cout << "I am a NeuronInput! W:"<<std::to_string(weight)<<", V:"<<std::to_string(value)<<std::endl;
	return weight*value;
}

float NeuronInput::getUnweightedValue() const{
	return value;
}
//set state
void NeuronInput::setInputValue(float input){
	value = input;
}
void NeuronInput::setWeight(float w){
	weight = w;
}
// get params
float NeuronInput::getWeight() const{
	return weight;
}
