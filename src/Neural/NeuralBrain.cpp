/*
 * NeuralBrain.cpp
 *
 *  Created on: Dec 30, 2023
 *      Author: robert
 */

#include "NeuralBrain.h"
#include <iostream>
#include <algorithm>


NeuralBrain::NeuralBrain(unsigned short numIn, unsigned short numOut):
dist(0.0,1.0),
numInputs(numIn),
numOutputs(numOut),
inputs(numIn),
outputs(numOut),
layers(),	// numHiddenLayers+2	// add input and output layers.
connections() //numHiddenLayers+1 // output nodes have no connections so only +1 instead of +2.
{
	// Reserve brainsize
	layers.reserve(numHiddenLayers+2);
	connections.reserve(numHiddenLayers+1);
    for (unsigned short i=0; i<numHiddenLayers+1; i++) {
        connections.push_back(std::vector<NeuralConnection>()); // Create an empty vector for each layer
    }
	// Populate neurons
	layers.push_back(NeuralLayer(numInputs));
	for(unsigned short i=0; i<numHiddenLayers;i++){
		layers.push_back(NeuralLayer(numNeuronsInHiddenLayer));
	}
	layers.push_back(NeuralLayer(numOutputs));
	// Add one input to each inputNeuron
	for(unsigned short i=0; i<numInputs;i++){
		layers[0].getNeuron(i).addInput();
		layers[0].getNeuron(i).getNeuronInput(0).setWeight(1.0); // Do not use random weights for world inputs.
	}

	//Reserve connection layers
	for(unsigned short i=0;i<connections.size();i++){
		std::vector<NeuralConnection>& conVec = connections[i];
		conVec.reserve(layers[i+1].getSize()*layers[i+1].getSize()); // Works since getSize returns allocated size
	}

	// Set up connections for fully connected
	unsigned short numLayers = layers.size();
	for(unsigned short layi=0;layi<numLayers;layi++){						// for all layers
		if(layi == 0){						 								// if inputlayer
			for(int i=0;i<numInputs;i++){					 				// for each input node
				for(int t=0;t<layers[1].getSize();t++){	 					// cycle through all nodes in next layer
					layers[1].getNeuron(t).addInput();						// (before connecting them, add reciever in target node)
					unsigned short inputIndex = layers[1].getNeuron(t).getNumInputs()-1; // dont miss -1, 0-indexing.
					connections[0].push_back(NeuralConnection(0,i,1,t,inputIndex));	// and connect them. This will be the first input for each node.
				}
			}
		}
		else if (layi == numLayers-1){   					 // if outputlayer
			// Output layer
			// We don't need to do anything here, I think. All connections are already made.
			// lets keep this comment here for clarity
		}
		else { // hidden layer
			NeuralLayer& thisLayer = layers[layi]; 												// Lets keep things easy to follow
			NeuralLayer& nextLayer = layers[layi+1];											// (safe bc check for output node)
			for(unsigned short i=0;i<thisLayer.getSize();i++){												// for each nodes in this layer
				for(unsigned short t=0;t<nextLayer.getSize();t++){ 										// connect to all nodes in next layer
					Neuron& targetNeuron = nextLayer.getNeuron(t);								//
					targetNeuron.addInput();													// new connection to target means new input
					unsigned short numInputsInThatNode = targetNeuron.getNumInputs();   		// get index to new input
					targetNeuron.getNeuronInput(numInputsInThatNode-1).setWeight(-1+2*dist.getRnd()); 	// assign random weight to this connection // note to future self: this has a profound effrect (bias) on the preffered direction of motion. Not sure why, but try setting it to 0-1 instead of -1to1 and see!
					connections[layi].push_back(NeuralConnection(layi, i, layi+1, t, numInputsInThatNode-1));
				}
			}
		}
	}

}

void NeuralBrain::setInputs(const std::vector<float>& in){
	if(in.size() != numInputs){ // In the future we may allow padding for shorter inputs
		throw std::runtime_error("Trying to assign an invalid amount of inputs");
	}

	// update local array for inputs, array might be surperflous
	for(unsigned short i=0;i<numInputs;i++){
		inputs[i] = in[i];
	}

	// Update input neuron states
	for(unsigned short i=0;i<numInputs;i++){ //Todo: normalize inputs? Clamp?
		layers[0].getNeuron(i).getNeuronInput(0).setInputValue(inputs[i]);
		layers[0].getNeuron(i).updateState();
	}
}

const std::vector<float>& NeuralBrain::getOutputs() const{
	return outputs;
}


void NeuralBrain::stepNet(){ // do we really iterate the way we think through layers here?
	for(auto& conlay : connections){ // Iterate through every layers connections
		for(auto& con : conlay){ 	 // Through each connection in that layer
			auto& sourceNeuron = layers[con.sourceLayerIx].getNeuron(con.sourceNeuronIx);
			sourceNeuron.updateState();
			float sourceState = sourceNeuron.getState();
			auto& targetNeuron = layers[con.targetLayerIx].getNeuron(con.targetNeuronIx);
			auto& targetNeuronInput = targetNeuron.getNeuronInput(con.targetInputIx);
			targetNeuronInput.setInputValue(sourceState); //Fire! (The neuron through connection that is)
		}
	}
	// All nodes which have a connection originating from them have been processed,
	// so now we manually update the exit neurons states and saving its output.
	for(unsigned short i=0;i<numOutputs;i++){
		layers.back().getNeuron(i).updateState();
		outputs[i] = layers.back().getNeuron(i).getState();
	}
}

void NeuralBrain::mutate(float amount){
	for(auto& conlay : connections){	// Go through all layers of connections
		for(auto& con : conlay){ 	 	// Through each connection in that layer
			auto& targetNeuron = layers[con.targetLayerIx].getNeuron(con.targetNeuronIx);	// Get target Neuron
			auto& targetNeuronInput = targetNeuron.getNeuronInput(con.targetInputIx);		// and the input associated with this connection
			auto tniw = targetNeuronInput.getWeight(); 										// the vale to modify
			float newWeight = tniw*(1-amount) + amount*dist.getRnd(); 						// simple mutation for now
			newWeight = std::clamp(newWeight,-1.0f,1.0f);									// Keep normalized
			targetNeuronInput.setWeight(newWeight);											// set it
		}
	}
}

void NeuralBrain::dumpState(){
	std::cout << "BrainDump" << std::endl;
	std::cout << "NumHidLayers: " << std::to_string(numHiddenLayers) << std::endl;
	std::cout << "numInputs: " << std::to_string(numInputs) << std::endl;
	std::cout << "numOutputs: " << std::to_string(numOutputs) << std::endl;
	std::cout << "size layers: " << std::to_string(layers.size()) << std::endl;
	std::cout << "size connections: " << std::to_string(connections.size()) << std::endl;
	std::cout << "Inputs: ";
	for(int i=0;i<numInputs;i++){
		std::cout << std::to_string(inputs[i]) << ", ";
	}
	std::cout << std::endl;
	std::cout << "Outputs: ";
	for(int i=0;i<numOutputs;i++){
		std::cout << std::to_string(outputs[i]) << ", ";
	}
	std::cout << std::endl;

	std::cout << "Brainstate: " << std::endl;
	for(unsigned short L=0;L<layers.size();L++){
		std::cout << "Layer " << std::to_string(L) << ", size:" << std::to_string(layers[L].getSize())<<std::endl;
		for(unsigned short i=0;i<layers[L].getSize() ;i++){
			std::cout << std::to_string(layers[L].getNeuron(i).getState());
			std::cout << "(";
			std::cout << std::to_string(layers[L].getNeuron(i).getNumInputs());
			//for(unsigned short j=0;i<layers[L].getNeuron(i).getNumInputs();j++){
			//	std::cout << std::to_string(layers[L].getNeuron(i).getNeuronInput(j).getUnweightedValue()) << "*\n";
			//	std::cout << std::to_string(layers[L].getNeuron(i).getNeuronInput(j).getWeight()) << ", ";
			//}
			std::cout << ")";
			//std::cout << ", \n";
		}
		std::cout << std::endl;
	}
}
