/*
 * NeuralBrain.h
 *
 *  Created on: Dec 25, 2023
 *      Author: robert
 */

#ifndef SRC_NEURALBRAIN_H_
#define SRC_NEURALBRAIN_H_

#include "Math/RandomDist.h"
#include "NeuralLayer.h"
#include "NeuronInput.h"
#include "NeuralConnection.h"
#include <vector>

class NeuralBrain {
private:
	RandomDist dist;
	const unsigned short numHiddenLayers = 2;
	const unsigned short numNeuronsInHiddenLayer = 8;
	const unsigned short numInputs; //=8;
	const unsigned short numOutputs; //=4;
	std::vector<float> inputs; // Real world inputs
	std::vector<float> outputs; // Real world outputs
	std::vector<NeuralLayer> layers;
	std::vector<std::vector<NeuralConnection>> connections; // one vector of connections for each layer except output layer

public:
	NeuralBrain(unsigned short numIn, unsigned short numOut);
	void setInputs(const std::vector<float>& in);
	const std::vector<float>& getOutputs() const;
	void dumpState();

	void stepNet();
	void mutate(float amount);

};


#endif /* SRC_NEURALBRAIN_H_ */


//inpts, 8:
// Energy
// seeing something
// distance
// RGB
// always on
// always off
//
//outputs, 4:
// speed fwd / back
// turn left / right
