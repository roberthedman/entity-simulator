/*
 * NeuralConnection.h
 *
 *  Created on: Jan 3, 2024
 *      Author: robert
 */

#ifndef SRC_NEURAL_NEURALCONNECTION_H_
#define SRC_NEURAL_NEURALCONNECTION_H_

class NeuralConnection{
public:
	const unsigned short sourceLayerIx,sourceNeuronIx;
	const unsigned short targetLayerIx, targetNeuronIx, targetInputIx;

	NeuralConnection(unsigned short sourceLayerIx,
					 unsigned short sourceNeuronIx,
					 unsigned short targetLayerIx,
					 unsigned short targetNeuronIx,
					 unsigned short targetInputIx);
};


#endif /* SRC_NEURAL_NEURALCONNECTION_H_ */
