
/*
 * NeuralLayer.h
 *
 *  Created on: Jan 1, 2024
 *      Author: robert
 */

#ifndef SRC_NEURAL_NEURALLAYER_H_
#define SRC_NEURAL_NEURALLAYER_H_

#include <vector>
#include "Neuron.h"

class NeuralLayer {
private:
	std::vector<Neuron> neurons;
	const unsigned short size;
public:
	//NeuralLayer();
	NeuralLayer(unsigned short numNeurons); // for reserving vector size
	unsigned short getSize() const; // returns size of neurons vector.
	Neuron& getNeuron(unsigned short index);
};


#endif /* SRC_NEURAL_NEURALLAYER_H_ */
