/*
 * NeuralConnection.cpp
 *
 *  Created on: Jan 3, 2024
 *      Author: robert
 */

#include "NeuralConnection.h"

NeuralConnection::NeuralConnection(unsigned short sourceLayerIx,
				 unsigned short sourceNeuronIx,
				 unsigned short targetLayerIx,
				 unsigned short targetNeuronIx,
				 unsigned short targetInputIx):
sourceLayerIx(sourceLayerIx),
sourceNeuronIx(sourceNeuronIx),
targetLayerIx(targetLayerIx),
targetNeuronIx(targetNeuronIx),
targetInputIx(targetInputIx)
{}

