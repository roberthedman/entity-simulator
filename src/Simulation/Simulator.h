/*
 * Simulator.h
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#include <SFML/Graphics.hpp>
#include <random>
#include <memory>

#include "Simulatables/Simulatable.h"
#include "Simulatables/Entity.h"
#include "Simulatables/Consumable.h"
#include "Simulation/VisibilityManager.h"

class Simulator {
private:
	std::vector<std::shared_ptr<Simulatable>> allSimulatables;
	std::vector<std::shared_ptr<Entity>> entities;
	std::vector<std::shared_ptr<Consumable>> consumables;
    std::random_device rd;
    std::mt19937 rng;
    sf::RenderWindow window;
    static const sf::Color worldColor;
    VisibilityManager vm;
    void addSimulatable(std::shared_ptr<Simulatable> s);
	int generation;
	int frameskip;
	int frame;

public:
    Simulator();

    void addEntity(std::unique_ptr<Entity> e);
    void addConsumable(std::unique_ptr<Consumable> c);
    void populateApples();
    void simulateStep();
    bool isWindowOpen() const;
    VisibilityManager& getVisibilityManager();
    sf::RenderWindow& getWindow();

    ~Simulator();
};


#endif /* SIMULATOR_H_ */
