/*
 * VisibilityManager.cpp
 *
 *  Created on: Dec 26, 2023
 *      Author: robert
 */
#include "Simulation/VisibilityManager.h"

#include <iostream>
#include "SFML/System/Vector2.hpp"
//#include "Entity.h"
#include "Math/VecOp.h"
#include <cmath>
#include <vector>

VisibilityManager::VisibilityManager(std::vector<std::shared_ptr<Simulatable>>& e): entities(e){}


VisibilityInfo VisibilityManager::getFirstVisibleObjectAlongLine(const sf::Vector2f& start, const sf::Vector2f& end){

	VisibilityInfo visInfo{false, sf::Color(0,0,0), 0.0, nullptr};

	sf::Vector2f sightLine = end-start;
	std::vector<Simulatable*> visibleToTest;

	// gather list of entities that could be visible
	for(auto& entity : entities){
		// skip ourselves, in ugly way
		if(entity->getPosition() == start){
			continue;
		}

		// This is another entity, get info
		sf::Vector2f ePos = entity->getPosition();
		sf::Vector2f fromSeerToE = ePos-start;

		// check if e in front of us, if not go to next entity.
		if(VecOp::dot(fromSeerToE, sightLine) < 0.0f){
			continue;
		}
		//check if e is visible
		bool visible = isIntersecting(start,end,entity->getBoundingCircle());
		if(visible){
			visibleToTest.push_back(entity.get());
		}
		else{
			continue;
		}
	}

	// If none found stop.
	//std::cout << "NumVisible: " << visibleToTest.size() << std::endl;
	if(visibleToTest.empty()){
		return visInfo;
	}

	//Then find closest, start with one to compare against
	auto testEntity = visibleToTest.back();
	visibleToTest.pop_back();
	visInfo.anything=true;
	setVisInfo(visInfo, testEntity, start);

	for( auto e : visibleToTest){
		if(VecOp::length(e->getPosition() - start) < visInfo.distance){
			setVisInfo(visInfo, e, start);
		}
	}
	//std::cout << "distance: " << visInfo.distance << std::endl;
	//std::cout << "color: "<<int(visInfo.color.r)<<", "<<int(visInfo.color.g)<< ", "<< int(visInfo.color.b)<< std::endl;
	return visInfo;

}

void VisibilityManager::setVisInfo(VisibilityInfo &vi, Simulatable* e, const sf::Vector2f& start){
	vi.color = e->getBoundingCircle().getFillColor();
	vi.distance = VecOp::length(e->getPosition() - start);
	vi.entity = e;
}


// This function can be optimized by only dealing with length squared units and skipping all square roots.
bool VisibilityManager::isIntersecting(const sf::Vector2f& start, const sf::Vector2f& end, const sf::CircleShape& circle) {
    sf::Vector2f circleCenter = circle.getPosition();
    float circleRadius = circle.getRadius();

    // Direction vector of the line
    sf::Vector2f sightLine = end - start;
    sf::Vector2f sightLineDir = VecOp::normalize(sightLine);

    // Vector from start of the line to the circle's center
    sf::Vector2f toCircle = circleCenter - start;

    // Project toCircle onto lineDir to find the closest point
    float projectionLength = VecOp::dot(toCircle, sightLine) / VecOp::length(sightLine) ;
    if (projectionLength < 0.0f || projectionLength > VecOp::length(sightLine)){ // circle is behind us or too far away
    	return false;
    }
    sf::Vector2f closestPoint = start + projectionLength*sightLineDir;

    // check if circle is overlapping
    return VecOp::length(closestPoint-circleCenter) <= circleRadius;
}

