/*
 * VisibilityManager.h
 *
 *  Created on: Dec 26, 2023
 *      Author: robert
 */

#ifndef SRC_VISIBILITYMANAGER_H_
#define SRC_VISIBILITYMANAGER_H_

#include <vector>
#include <memory>
#include "Simulatables/Simulatable.h"
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Color.hpp>

struct VisibilityInfo {
	bool anything;
	sf::Color color;
	float distance;
	Simulatable* entity;
};

class VisibilityManager {
private:
	std::vector<std::shared_ptr<Simulatable>>& entities;
	Simulatable* getClosestVisible();
	bool isIntersecting(const sf::Vector2f& start, const sf::Vector2f& end, const sf::CircleShape& circle);
	void setVisInfo(VisibilityInfo &vi, Simulatable* e, const sf::Vector2f& start);

public:

	VisibilityManager(std::vector<std::shared_ptr<Simulatable>>& entities);
	VisibilityInfo getFirstVisibleObjectAlongLine(const sf::Vector2f& start, const sf::Vector2f& end);
};


#endif /* SRC_VISIBILITYMANAGER_H_ */
