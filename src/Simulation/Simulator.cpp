/*
 * Simulator.cpp
 *
 *  Created on: Dec 24, 2023
 *      Author: robert
 */

#include "Simulator.h"
#include <vector>
#include <iostream>
#include "Simulatables/Apple.h"
#include "Simulatables/SeeingEntity.h"

const sf::Color Simulator::worldColor = sf::Color(0,100,0);

Simulator::Simulator() :
allSimulatables(),
entities(),
consumables(),
rd(),
rng(rd()),
window(sf::VideoMode(2000, 1400), "Simulator"),
vm(allSimulatables),
generation(0),
frameskip(1),
frame(0)
{
	window.clear(worldColor);
}

void Simulator::populateApples(){
	addConsumable(std::make_unique<Apple>(Apple(400,400))); // add one off apple
	// add lots of apples in a grid
	for(int i=0;i<26;i++){
		for(int j=0;j<18;j++){
			addConsumable(std::make_unique<Apple>(Apple(100+70*i,150+70*j)));
		}
	}
}

void Simulator::simulateStep() {
	window.clear(worldColor);

	// randomize order we update entities in for fairer playground
	std::shuffle(allSimulatables.begin(), allSimulatables.end(), rng);

	// Remove dead entities
	consumables.erase(std::remove_if(consumables.begin(), consumables.end(),
						   [](const std::shared_ptr<Consumable>& c) {
								return !c->isAlive();
						   }), consumables.end());
	entities.erase(std::remove_if(entities.begin(), entities.end(),
							   [](const std::shared_ptr<Entity>& e) {
									return !e->isAlive();
							   }), entities.end());
	allSimulatables.erase(std::remove_if(allSimulatables.begin(), allSimulatables.end(),
					   [](const std::shared_ptr<Simulatable>& s) {
							return !s->isAlive();
					   }), allSimulatables.end());


	//std::cout << std::to_string(entities.size()) << std::endl;
	if (entities.size() <= 10) {
		generation++;
		std::cout << std::to_string(generation) << std::endl;
		// stop simulation, slightly mutate the remaining entitis, create 90 new, reset 10 left, then start next generation.

		// kill all consumables so that they can be pruned from allSimulatables
		for(auto c : consumables){
			c->consume();
		}
		consumables.clear();
		allSimulatables.erase(std::remove_if(allSimulatables.begin(), allSimulatables.end(), //prune
							   [](const std::shared_ptr<Simulatable>& s) {
									return !s->isAlive();
							   }), allSimulatables.end());

		// reset already existing entities
		for(auto& e : entities){
			e->reset();
		}

		if(entities.size()==0){
			std::cout << "All entities died, creating new from scratch." << std::endl;
			for(int i=0;i<20;i++){
	    		addEntity(std::make_unique<SeeingEntity>(SeeingEntity(getVisibilityManager(), "Seer"+std::to_string(i), 80*i+350, 250)));

			}
		}

		// now create copies of all survivors, mutate their brains
		std::vector<std::unique_ptr<SeeingEntity>> newSimulatables;
		for(auto& e : entities){
			auto se = std::dynamic_pointer_cast<SeeingEntity>(e);
			for(int i=0;i<9;i++){
				auto newE = std::make_unique<SeeingEntity>(SeeingEntity(getVisibilityManager(), "Seer", 0,0, se->getMutatedBrain(i*0.1)));
				newSimulatables.push_back(std::move(newE));
			}
		}

		for (auto&& e: newSimulatables){
			addEntity(std::move(e));
		}

		// now set new positions for all
		std::vector<std::pair<float,float>> positions;
		for(int i=0;i<10;i++){
		    	for(int j=0;j<10;j++){
		    		positions.push_back(std::pair<float,float>(float(80*i+350),float(150+90*j)));
		    	}
		    }
		for (auto& e: entities){
			e->setPosition(positions.back().first, positions.back().second);
			positions.pop_back();
		}
		positions.clear();
		populateApples();
	}

    // Update all alive entities.
	for ( std::shared_ptr<Simulatable>& s : allSimulatables){
		s->step();
		if(frame==frameskip){
			s->draw(window);
		}
	}


	if(frame==frameskip){
		window.display();
		frame=0;
	}
	++frame;

}

bool Simulator::isWindowOpen() const {
	return window.isOpen();
}

sf::RenderWindow& Simulator::getWindow() {
	return window;
}

VisibilityManager& Simulator::getVisibilityManager(){
	return vm;
}

Simulator::~Simulator() {
	allSimulatables.clear();
	entities.clear();
	consumables.clear();
}

void Simulator::addSimulatable(std::shared_ptr<Simulatable> s) {
	allSimulatables.push_back(std::move(s));
}
void Simulator::addEntity(std::unique_ptr<Entity> e) { // Could the vector move the entity and cause troubles?
	// We will now own the object directly, no unique_ptr.
	Entity* entp = e.release(); // DANGER DANGER! Lets manage this carefully... Quick, create a shared pointer!
	std::shared_ptr<Entity> sharedE(entp);
	entities.push_back(sharedE);
	addSimulatable(std::move(sharedE));
}
void Simulator::addConsumable(std::unique_ptr<Consumable> c) {
	// We will now own the object directly, no unique_ptr.
	Consumable* cp = c.release(); // DANGER DANGER! Lets manage this carefully... Quick, create a shared pointer!
	std::shared_ptr<Consumable> sharedC(cp);
	consumables.push_back(sharedC);
	addSimulatable(std::move(sharedC));
}


